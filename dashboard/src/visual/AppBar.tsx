import * as React from 'react';
import { AppBar, ToggleThemeButton } from 'react-admin';
import { Typography } from '@mui/material';

import LogoutButton from '../authProvider/logoutButton';
import { darkTheme, lightTheme } from './Theme';


const CustomAppBar = (props: any) => {
    return (
        <AppBar
            {...props}
            color="primary"
            elevation={1}
            userMenu={<LogoutButton/>}

        >
            <Typography
                align='left'
                variant="h6"
                color="inherit"
                sx={{
                    flex: 1,
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                }}
                id="react-admin-title"
            />
            <ToggleThemeButton
                lightTheme={lightTheme}
                darkTheme={darkTheme}
            />
        </AppBar>
    );
};

export default CustomAppBar;
