import { defaultTheme } from 'react-admin';

export const darkTheme = {
    palette: {
        primary: {
            main: '#e6dfdf',
        },
        secondary: {
            main: '#f20c3e',
        },
        mode: 'dark' as 'dark',
    },
    sidebar: {
        width: 200,
    },
    components: {
        ...defaultTheme.components,
        RaMenuItemLink: {
            styleOverrides: {
                root: {
                    borderLeft: '3px solid #000',
                    '&.RaMenuItemLink-active': {
                        borderLeft: '3px solid #e6dfdf',
                    },
                },
            },
        },
        MuiAppBar: {
            styleOverrides: {
                colorSecondary: {
                    color: 'rgba(250, 250, 250, 0.95)',
                    backgroundColor: 'rgba(97, 97, 97, 0.90)',
                },
            },
        },
    },
};

export const lightTheme = {
    palette: {
        primary: {
            main: '#00bbff',
        },
        secondary: {
            light: '#5abae0',
            main: '#173d7a',
            dark: '#010c4a',
            contrastText: '#fff',
        },
        background: {
            default: '#fcfcfe',
        },
        mode: 'light' as 'light',
    },
    shape: {
        borderRadius: 10,
    },
    sidebar: {
        width: 200,
    },
    components: {
        ...defaultTheme.components,
        RaMenuItemLink: {
            styleOverrides: {
                root: {
                    borderLeft: '3px solid #fff',
                    '&.RaMenuItemLink-active': {
                        borderLeft: '3px solid #00bbff',
                    },
                },
            },
        },
        MuiPaper: {
            styleOverrides: {
                elevation1: {
                    boxShadow: 'none',
                },
                root: {
                    border: '1px solid #e0e0e3',
                    backgroundClip: 'padding-box',
                },
            },
        },
        MuiAppBar: {
            styleOverrides: {
                colorSecondary: {
                    color: '#808080',
                    backgroundColor: 'rgba(240, 240, 240, 0.90)',
                },
            },
        },
        MuiLinearProgress: {
            styleOverrides: {
                colorPrimary: {
                    backgroundColor: '#fafafa',
                },
                barColorPrimary: {
                    backgroundColor: '#d7d7d7',
                },
            },
        },
        MuiTableRow: {
            styleOverrides: {
                root: {
                    '&:last-child td': { border: 0 },
                },
            },
        },
    },
};

/*
import * as React from 'react';
import Button from '@mui/material/Button';
import { useTheme, Title } from 'react-admin';

import { darkTheme, lightTheme } from './Theme';

const SwitchTheme = () => {
    const [theme, setTheme] = useTheme();

    return (
        <Button onClick={() => setTheme(lightTheme ? darkTheme : lightTheme)}>
            {darkTheme ? 'Switch to light theme' : 'Switch to dark theme'}
        </Button>
    );
}

export default SwitchTheme;
*/