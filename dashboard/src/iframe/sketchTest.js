// based on dataset_1.json
let data = {}; // Global object to hold results from the loadJSON call
let bubbles = []; // Global array to hold all bubble objects
let sqrList = []; // list of job in square ellement

// Put any asynchronous data loading in preload to complete before "setup" is run
function preload() {
    data = loadJSON('./dataset_1.json');
    console.log(data);
}

let resources;
function loadBubble() {
    resources = data['data'][4]['records'];
    console.log("sqrList : ", sqrList);
    resources.forEach(element => {
        let str = element['host'];
        const last = parseInt(str.charAt(str.length - 1));
        console.log("node : " + last + " ; res id : " + element['id']);
        let y = 1;
        let id = element['id'];
        if (id > resources.length / 2) {
            y = 3;
            id -= resources.length / 2
        }
        // sqrList = [{ 'job': { 'id': 1, 'array_id': 1 } },
        //     { 'job': { 'id': 2, 'array_id': 1 } },
        //     { 'job': { 'id': 3, 'array_id': 1 } },
        //     { 'job': { 'id': 4, 'array_id': 4 } },
        //     { 'job': { 'id': 5, 'array_id': 5 } },
        //     { 'job': { 'id': 6, 'array_id': 6 } },
        //     { 'job': { 'id': 7, 'array_id': 5 } },
        //     { 'job': { 'id': 8, 'array_id': 12 } },
        //     { 'job': { 'id': 9, 'array_id': 5 } },
        //     { 'job': { 'id': 10, 'array_id': 1 } }
        // ];
        let nodeJobs = [];
        for (i = 0; i < sqrList.length; i++){
            if (sqrList[i]['job']['array_id'] == element['id']) {
                nodeJobs.push(sqrList[i]);
            }
        }
        console.log(nodeJobs);
        bubbles.push(new Bubble(id*(windowWidth/(resources.length/1.7)), 200*y, 100, element['id'],nodeJobs));
    });
}


function loadJobs() {
    let x = 50;
    let y = 50;
    let JobsList = data['data'][9]['records'];
    // console.log(JobsList);
    JobsList = JobsList.sort(function(first, second) {
        return first.id - second.id;
    });
    JobsList.forEach(element => {
        x += 50;
        sqrList.push(new SquareJob(x, y, 50,element));
    });
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    loadJobs();
    loadBubble();
    
}

function draw() {
    background(150);
    // loadJobs();
    for (let i = 0; i < sqrList.length; i++) {
        sqrList[i].display();
        sqrList[i].rollover(mouseX,mouseY);
    }
    // Display all bubbles
    for (let i = 0; i < bubbles.length; i++) {
        bubbles[i].display();
        bubbles[i].rollover(mouseX, mouseY);
    }
}

function actionOver(SqrJob) {
    SqrJob.over();
}


function mousePressed() {
    for (let i = 0; i < bubbles.length; i++) {
    bubbles[i].clicked();
  }
}


