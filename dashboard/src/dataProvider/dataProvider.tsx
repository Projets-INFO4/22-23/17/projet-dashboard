import { DataProvider, fetchUtils } from 'react-admin';

import * as ra from '../../types/react-admin';
import { AuthRequest } from '../authProvider/authRequester';

import { GetListResult } from 'react-admin';

const apiUrl: string = '/oarapi';
const apiPrivUrl: string = '/oarapi-priv'

const httpClient = fetchUtils.fetchJson;

/**
 * The data provider is the glue layer between the oar API and the application.
 * Several CRUD methods are defined.
 */
export let myDataProvider: ra.DataProvider = {

    async getListJobs(
        filter: any,
        sorter: (a: ra.Record, b: ra.Record) => number, page: number, perPage: number):
        Promise<ra.GetListResult> {

        let headers: Headers = new Headers();
        headers.append('Content-type', 'application/json');

        // API available filters for /jobs
        let options: string = '?';
        if ('array' in filter)
            options += '&array=' + filter['array'].toString();
        if ('state' in filter)
            options += '&state=' + filter['state'].toString();
        if ('owner' in filter)
            options += '&owner=' + filter['owner'].toString();
        if ('ids' in filter)
            options += '&ids=' + filter['ids'].toString().replace(/,/g, ':');
        if ('from' in filter)
            options += '&from=' + filter['from'].toString();
        if ('to' in filter)
            options += '&to=' + filter['to'].toString();

        // Pagination
        options += '&limit=' + perPage + '&offset=' + perPage * (page - 1);

        return httpClient(`${apiUrl}/jobs` + options, { headers: headers }).then(
            function ({ json }): ra.GetListResult {
                // Sorting
                let data: ra.Record[] = json.items.sort(sorter);

                return {
                    data: data,
                    total: json.total
                };
            }
        );
    },

    async getListResource(
        filter: any,
        sorter: (a: ra.Record, b: ra.Record) => number, page: number,
        perPage: number):
        Promise<ra.GetListResult> {

        let headers: Headers = new Headers();
        headers.append('Content-type', 'application/json');

        let options: string = '?';
        // Pagination
        options += '&limit=' + perPage + '&offset=' + perPage * (page - 1);

        return httpClient(`${apiUrl}/resources` + options, { headers: headers }).then(
            function ({ json }): ra.GetListResult {
                // Sorting (after fetch so it's a "by page" sorting which is not ideal)
                let data: ra.Record[] = json.items.sort(sorter);

                return {
                    data: data,
                    total: json.total
                };
            }
        );
    },

    // Will not work properly with filters not supported by oar api
    async getList(resource: string, params: ra.GetListParams)/*:Promise<any> */{

        // Decompositions of parameters
        let filter = params.filter;
        let sortField: string = params.sort.field;
        let sortOrder: string = params.sort.order;
        let sorter = function (a: ra.Record, b: ra.Record): number {
            if (a[sortField] > b[sortField]) {
                return sortOrder === "ASC" ? 1 : -1;
            } else if (a[sortField] < b[sortField]) {
                return sortOrder === "ASC" ? -1 : 1;
            }
            return 0;
        }
        let page: number = params.pagination.page;
        let perPage: number = params.pagination.perPage;

        // Treat differenlty, different kind of resources
        if (resource === 'jobs')
            return this.getListJobs(filter, sorter, page, perPage);
        if (resource === 'resources')
            return this.getListResource(filter, sorter, page, perPage);

        return { data: [], total: 0 };
        // return Promise.resolve({ data: [], total: 0 })
    },

    async getOne(resource: string, params: ra.GetOneParams)/*: Promise<any>*/  {
        let headers: Headers = new Headers();
        headers.append('Content-type', 'application/json');

        let options: string = '';
        if (resource === 'jobs')
            options += '/details';
        return httpClient(`${apiUrl}/${resource}/${params.id}` + options, { headers: headers }).then(({ json }) => ({
            data: json,
        }));
        //return Promise.resolve({ data: {} })
    },

    async getMany(resource: string, params: ra.GetManyParams)/*: Promise<any>*/ {
        let headers: Headers = new Headers();
        headers.append('Content-type', 'application/json');

        let options: string = 'ids=' + params.ids.toString().replace(',', ':');
        return httpClient(`${apiUrl}/${resource}?` + options, { headers: headers }).then(
            function ({ json }){
                if (resource === 'jobs')
                    return { data: json.items };
                else
                    return { data: json.items.filter(item => params.ids.includes(item.id)) }
            }
        );
        //return Promise.resolve({ data: [] })
    },

    async getManyReference(resource: string, params: ra.GetManyReferenceParams)/*: Promise<any>*/ {        
        
        const fil = params.filter;

        const id: ra.Identifier = params.id;
        const target: string = params.target;
        fil[target] = id;

        return this.getList(resource, { pagination: params.pagination, sort: params.sort, filter: fil }).then(
            function (a: GetListResult<any>): ra.GetManyReferenceResult<any> {
                return a as ra.GetManyReferenceResult<any>;
            }
        )
        //return Promise.resolve({ data: { id: 0 } })
    },

    async update(resource: string, params: ra.UpdateParams)/*: Promise<any>*/ {
        let options: string = '';
        if (resource === "resources")
            options += '/state';

        return AuthRequest(`${apiPrivUrl}/${resource}/${params.id}` + options, {
            method: 'POST',
            header: 'Content-type: application/json',
            body: JSON.stringify({ 'state': params.data.state }),
        }).then(function ({ json }): ra.UpdateResult {
            return { data: json };
        }
        );
        //return Promise.resolve({ data: {} })
    },

    async updateMany(resource: string, params: ra.UpdateManyParams)/*: Promise<any> */{
        let promises: Promise<ra.UpdateResult>[] = new Array(params.ids.length);

        // The oar API cannot update multiple resources at one so we call update multiple times.
        params.ids.forEach(element => {
            if (element !== undefined)
                promises.push(this.update(resource, { id: element, data: { state: params.data.state }, previousData: { id: element } }));
        });

        return Promise.all(promises).then(function (values: ra.UpdateResult[]): ra.UpdateManyResult {
            let ids: ra.Identifier[] = new Array(values.length);

            values.forEach(element => {
                if (element !== undefined) {
                    let tmp: ra.Identifier | undefined = element.data?.id;
                    if (tmp !== undefined)
                        ids.push(tmp)
                }
            });

            return { data: ids };
        });
        //return Promise.resolve({ data: [] })
    },

    async create(resource: string, params: ra.CreateParams)/*: Promise<any> */{
        return AuthRequest(`${apiPrivUrl}/${resource}`, {
            method: 'POST',
            header: 'Content-type: application/json',
            body: JSON.stringify(params.data),
        }).then(
            function ({ json }): ra.CreateResult {
                return {
                    data: { ...params.data, id: json.id }
                }
            }
        );
        //return Promise.resolve({ data: { id: 0 } })
    },

    async delete(resource: string, params: ra.DeleteParams)/*: Promise<any> */ {
        return AuthRequest(`${apiPrivUrl}/${resource}/${params.id}`, { //id -> ids
            method: 'DELETE',
            header: 'Content-type: application/json',
        }).then(function ({ json }): ra.DeleteResult {
            return { data: json };
        }
        );
        //return Promise.resolve({ data: {} })
    },

    async deleteMany(resource: string, params: ra.DeleteManyParams)/*: Promise<any>*/ {
        let promises: Promise<ra.DeleteResult>[] = new Array(params.ids.length);

        // The oar API cannot delete multiple resources at one so we call delete multiple times.
        params.ids.forEach(element => {
            if (element !== undefined)
                promises.push(this.delete(resource, { id: element }));
        });

        return Promise.all(promises).then(function (values: ra.DeleteResult[]): ra.DeleteManyResult {
            let ids: ra.Identifier[] = new Array(values.length);

            values.forEach(element => {
                if (element !== undefined) {
                    let tmp: ra.Identifier | undefined = element.data?.id;
                    if (tmp !== undefined)
                        ids.push(tmp)
                }
            });

            return { data: ids };
        });
        //return Promise.resolve({ data: [] })
    },
};
