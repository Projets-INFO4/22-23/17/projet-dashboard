import React from 'react';
import {
    Show, SimpleShowLayout, TextField, DateField, List, Datagrid, EditButton, Filter,
    Create, DateTimeInput, TabbedForm, TextInput, FormTab, NumberInput
} from 'react-admin';
import { Typography, Grid } from '@mui/material';

import { makeStyles } from '@mui/styles';

// The datagrid : the main job view
export const jobDataGrid = (props) => (
    <List filters={<JobFilter />} {...props}>
        <Datagrid rowClick="show">
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="state" />
            <DateField source="submission" />
            <EditButton />
        </Datagrid>
    </List>
);

// Enable job filtering by Id or state
const JobFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Id" source="ids" />
        <TextInput label="State" source="state" />
    </Filter>
);

export const drawGantt = (props) => (
  <iframe src="http://localhost:8000/drawgantt" title='drawGantt' height="700px" width="100%" ></iframe>  
);

export const monika = (props) => (
  <iframe src="http://localhost:8000/monika" title='monika' height="700px" width="100%" ></iframe>  
);

export const p5js = (props) => (
  <iframe src="http://127.0.0.1:5501/dev/projet-dashboard/dashboard/src/iframe/index.html" title='p5js' height="700px" width="100%" ></iframe>  
);

const UseStyles = makeStyles({
    root: {
        flexGrow: 1,
        width: 'unset'
    },
});

// Create form for a job
export const jobCreate = props => {

    const classes = UseStyles();

    return (
        <Create {...props} margin="normal" redirect="show">
            <TabbedForm>
                <FormTab label="Essentials" >
                    <Grid
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="flex-start"
                        spacing={2}
                        className={classes.root}>
                        {/* first column */}
                        <Grid item xs={5} >
                            <TextInput source="name" label="Name" />

                            <Typography variant="h6" gutterBottom>Resources</Typography>

                            <NumberInput source="switch" label="Number of switches" />
                            <NumberInput source="node" label="Number of nodes" />
                            <NumberInput source="cpu" label="Number of cpu" />
                            <NumberInput source="core" label="Number of cores" />
                        </Grid>
                        {/* second column */}
                        <Grid item xs={5}>
                            <Typography variant="h6" gutterBottom>Date & Duration</Typography>

                            <DateTimeInput source="reservation" label="Date" />
                            <TextInput source="walltime" label="Duration hh:mm:ss" />

                            <Typography variant="h6" gutterBottom>Task & Environment</Typography>

                            <TextInput source="command" label="Command" />
                            <TextInput source="workdir" label="Directory" />
                        </Grid>
                    </Grid>
                </FormTab>
                <FormTab label="Miscellaneous">
                    <TextInput source="optional" label="Optional command (do nothing yet)" />
                </FormTab>
            </TabbedForm>
        </Create>
    );
};

// Show view for a job, work in progress 
export const jobShow = props => (
    <Show title="Resource detail" {...props}>
        <SimpleShowLayout>
            <TextField label="ID" source="id" />
            <TextField label="Work in progress" source="cpu" />
        </SimpleShowLayout>
    </Show>
);
