import React, { FunctionComponent } from 'react';

/* USELESS */

/*
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import CircularProgress from '@mui/material/CircularProgress';
import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import { useTranslate, useLogin, useNotify, useSafeSetState } from 'ra-core';

interface Props {
	redirectTo?: string;
}

interface FormData {
	username: string;
	password: string;
}

const useStyles = makeStyles(
	(theme: Theme) => ({
		form: {
			padding: '0 1em 1em 1em',
		},
		input: {
			marginTop: '1em',
		},
		button: {
			width: '100%',
		},
		icon: {
			marginRight: theme.spacing(1),
		},
	}),
	{ name: 'RaLoginForm' }
);

const Input = ({
	meta: { touched, error }, // eslint-disable-line react/prop-types
	input: inputProps, // eslint-disable-line react/prop-types
	...props
}) => (
		<TextField
			error={!!(touched && error)}
			helperText={touched && error}
			{...inputProps}
			{...props}
			fullWidth
		/>
	);


//Copy-Pasted from the default one, but needs to be in this folder to enable the loginPage to work well
const LoginForm: FunctionComponent<Props> = ({ redirectTo }) => {
	const [loading, setLoading] = useSafeSetState(false);
	const login = useLogin();
	const translate = useTranslate();
	const notify = useNotify();
	const classes = useStyles({});

	const validate = (values: FormData) => {
		const errors = { username: undefined, password: undefined };

		if (!values.username) {
			errors.username = translate('ra.validation.required') as unknown as undefined;
		}
		if (!values.password) {
			errors.password = translate('ra.validation.required') as unknown as undefined;
		}
		return errors;
	};

	const submit = values => {
		setLoading(true);
		login(values, redirectTo)
			.then(() => {
				setLoading(false);
			})
			.catch(error => {
				setLoading(false);
				notify(
					typeof error === 'string'
						? error
						: typeof error === 'undefined' || !error.message
							? 'ra.auth.sign_in_error'
							: error.message,
				);
			});
	};
		const { register, handleSubmit } = useForm();
		const handleRegistration = (data) => console.log(data);
		return (
			<form onSubmit={submit}>
				validate={validate}
				render={({ handleSubmit }) => (
				<form onSubmit={handleSubmit(handleRegistration)} noValidate>
					<div className={classes.input}>
						autoFocus
						id="username"
						name="username"
						component={Input}
						label={translate('ra.auth.username')}
						disabled={loading}
					</div>
					<div className={classes.input}>
						id="password"
						name="password"
						component={Input}
						label={translate('ra.auth.password')}
						type="password"
						disabled={loading}
						autoComplete="current-password"
					</div>
					<CardActions>
						<Button
							variant="contained"
							type="submit"
							color="primary"
							className={classes.button}
						>
							{loading && (
								<CircularProgress
									className={classes.icon}
									size={18}
									thickness={2}
								/>
							)}
							{translate('ra.auth.sign_in')}
						</Button>
					</CardActions>
				</form>
				)}
			</form>
		)
};

LoginForm.propTypes = {
	redirectTo: PropTypes.string,
};

export default LoginForm;
*/