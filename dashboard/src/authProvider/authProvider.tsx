import * as ra from '../../types/react-admin';
import { fetchUtils } from 'react-admin';
import { AuthRequest } from './authRequester';

const httpClient = fetchUtils.fetchJson;
const apiUrl: string = '/oarapi';

export let myAuthProvider: ra.AuthProvider = {

	// called when the user attempts to log in
	async login({ username, password }): Promise<any> {

		localStorage.removeItem('redirect_to_login');

		return httpClient(`${apiUrl}/authentication?basic_user=${username}&basic_password=${password}`).then(response => {
			if (response.status !== 400 && response.status !== 400 && response.status !== 403) {

				localStorage.setItem('username', username);
				localStorage.setItem('password', password);
				localStorage.setItem('is_connected', 'true');

				if (localStorage.getItem('abortedRequestUrl') != null){ //Request started before login and which should be done after

					const abortedUrl : string = localStorage.getItem('abortedRequestUrl') || '';
					const abortedParams : any = JSON.parse(localStorage.getItem('abortedRequestParams') || '{}');
					localStorage.removeItem('abortedRequestUrl');
					localStorage.removeItem('abortedRequestParams');

					AuthRequest(abortedUrl, abortedParams).catch(function(){
						console.log("You can't perform this action as "+username);} //Improvement to do : should be notify
					);
				}
			}
		});
	},

	// called when the user clicks on the logout button
	async logout(): Promise<string | void> {

		let redirect_to_login: string | null = localStorage.getItem('redirect_to_login');

		if (redirect_to_login == null) { //Update the credentials only if the logout is triggered by the logout button, not by the login redirection
			localStorage.removeItem('username');
			localStorage.removeItem('password');
			localStorage.removeItem('is_connected');
		}
		return Promise.resolve();
	},
	// called when the API returns an error
	async checkError({ status }): Promise<void> {

		if (status === 400 || status === 401 || status === 403) {
			localStorage.setItem('redirect_to_login', 'true');
			return Promise.reject();
		}
		else {
			return Promise.resolve();
		}
	},
	// called when the user navigates to a new location, to check for authentication
	async checkAuth(): Promise<void> {

		let redirect_to_login: string | null = localStorage.getItem('redirect_to_login');

		if (redirect_to_login != null) {
			return Promise.reject();
		}
		else {
			return Promise.resolve();
		}
	},
	// called when the user navigates to a new location, to check for permissions / roles
	async getPermissions(): Promise<any> {
		return Promise.resolve();
	},
};
