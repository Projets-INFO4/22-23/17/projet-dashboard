import React, { forwardRef } from 'react';
import { useLogout } from 'react-admin';
import MenuItem from '@mui/material/MenuItem';
import ExitIcon from '@mui/icons-material/PowerSettingsNewTwoTone';
import PersonIcon from '@mui/icons-material/Person3TwoTone';

import { useNavigate } from 'react-router-dom';	//import { useHistory } from 'react-router-dom';

const LogoutButton: React.ForwardRefExoticComponent<React.RefAttributes<unknown>>
	= forwardRef((props, ref) => {
		const logout = useLogout();
		const handleClick = () => logout();
		
		const navigate = useNavigate();	//const history = useHistory();

		const routeToLogin = () => {
			localStorage.setItem('redirect_to_login', 'true');
			let path = '/login';
			
			navigate(path);	//history.push(path);
		}

		if (localStorage.getItem('is_connected') === 'true') {
			return (
				<MenuItem onClick={handleClick} >
					<ExitIcon /> Logout
					<PersonIcon /> Connected as: {localStorage.getItem('username')}
				</MenuItem>
			);
		}
		else {
			return (
				<MenuItem onClick={routeToLogin} >
					<PersonIcon /> Login
				</MenuItem>
			);
		}
	});

export default LogoutButton;