import React from 'react';
import { Admin, Resource } from 'react-admin';

import { myAuthProvider } from './authProvider/authProvider'
import LoginPage from './authProvider/loginPage'

import { drawGantt, monika, jobCreate, jobDataGrid, jobShow, p5js } from './dataProvider/job';
import { resourceDataGrid, resourceEdit } from './dataProvider/resource';
import { myDataProvider } from './dataProvider/dataProvider'

import { lightTheme } from './visual/Theme';
import Layout from './visual/Layout';

import * as ra from '../types/react-admin';

import resourceIcon from '@mui/icons-material/Memory';
import jobIcon from '@mui/icons-material/Work';
import TableChartTwoToneIcon from '@mui/icons-material/TableChartTwoTone';
import BackupTableTwoToneIcon from '@mui/icons-material/BackupTableTwoTone';
import SsidChartTwoToneIcon from '@mui/icons-material/SsidChartTwoTone';

//import loader from './loader.js'

//const dataProvider : ra.DataProvider = myDataProvider;
const authProvider: ra.AuthProvider = myAuthProvider;

const App = () => (
    // <iframe src="http://gist-it.appspot.com/https://github.com/oar-team/oar3/blob/master/visualization_interfaces/DrawGantt-SVG/test.svg" title='test' height="700px" width="50%" ></iframe> 

    <Admin 
    dataProvider={myDataProvider}
    authProvider={authProvider} 
    loginPage={LoginPage}
    theme={lightTheme}
    layout={Layout}
    >
        <Resource name="jobs" list={jobDataGrid} create={jobCreate} show={jobShow} icon={jobIcon}/>
        <Resource name="resources" list={resourceDataGrid} edit={resourceEdit} icon={resourceIcon} />
        <Resource name="Drawgantt" list={drawGantt} icon={TableChartTwoToneIcon}/>
        <Resource name="Monika" list={monika} icon={BackupTableTwoToneIcon} />
        <Resource name="P5js"  list={p5js} icon={SsidChartTwoToneIcon} />
    </Admin>
);

export default App;
