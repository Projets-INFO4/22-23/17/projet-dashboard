// declare module 'react-admin';

import { ReactNode, ReactElement, ComponentType } from 'react';
import {
    RouteProps,
    //RouteComponentProps,
    //match as Match,
} from 'react-router-dom';
import { Location, History } from 'history';
import { RecordContext } from 'react-admin';

import { WithPermissionsChildrenParams } from './auth/WithPermissions';
import { AuthActionType } from './auth/types';

/**
 * data types
 */

export type Identifier = string | number;

export interface Record {
    id: Identifier;
    [key: string]: any;
}

export interface RecordMap<RecordType = Record> {
    // Accept strings and numbers as identifiers
    [id: string]: RecordType;
    [id: number]: RecordType;
}

export interface Sort {
    field: string;
    order: string;
}
export interface Pagination {
    page: number;
    perPage: number;
}

/**
 * i18nProvider types
 */

export const I18N_TRANSLATE = 'I18N_TRANSLATE';
export const I18N_CHANGE_LOCALE = 'I18N_CHANGE_LOCALE';

export type Translate = (key: string, options?: any) => string;

export type I18nProvider = {
    translate: Translate;
    changeLocale: (locale: string, options?: any) => Promise<void>;
    getLocale: () => string;
    [key: string]: any;
};

/**
 * authProvider types
 */

export type AuthProvider = {
    login: (
        params: any
    ) => Promise<{ redirectTo?: string | boolean } | void | any>;
    logout: (params: any) => Promise<void | string>;
    checkAuth: (params: any) => Promise<void>;
    checkError: (error: any) => Promise<void>;
    getPermissions: (params: any) => Promise<any>;
    [key: string]: any;
};
/*
export type LegacyAuthProvider = (
    type: AuthActionType,
    params?: any
) => Promise<any>;
*/

/**
 * dataProvider types
 */

export type DataProvider<ResourceType extends string = string> = {
    getList: <RecordType extends Record = any> (
        resource: string, params: GetListParams
    ) => Promise<GetListResult<RecordType>>;

    getOne: <RecordType extends Record = any> (
        resource: string, params: GetOneParams
    ) => Promise<GetOneResult<RecordType>>;

    getMany: <RecordType extends Record = any> (
        resource: string, params: GetManyParams
    ) => Promise<GetManyResult<RecordType>>;

    getManyReference: <RecordType extends Record = any> (
        resource: string, params: GetManyReferenceParams    
    ) => Promise<GetManyReferenceResult<RecordType>>;

    update: <RecordType extends Record = any> (
        resource: string, params: UpdateParams
    ) => Promise<UpdateResult>;

    updateMany: <RecordType extends Record = any> (
        resource: string, params: UpdateManyParams
    ) => Promise<UpdateManyResult<RecordType>>;

    create: <RecordType extends Record = any> (
        resource: string, params: CreateParams
    ) => Promise<CreateResult<RecordType>>;

    delete: <RecordType extends Record = any> (
        resource: string, params: DeleteParams
    ) => Promise<DeleteResult<RecordType>>;

    deleteMany: <RecordType extends Record = any> (
        resource: string, params: DeleteManyParams
    ) => Promise<DeleteManyResult<RecordType>>;

    [key: string]: any;
};

export interface GetListParams {
    pagination: Pagination;
    sort: Sort;
    filter: any;
}
export interface GetListResult <RecordType extends Record = any> {
    data: RecordType[];
    total: number;
}

export interface GetOneParams <RecordType extends Record = any> {
    id: RecordType['id'];
    //id: Identifier;
}
export interface GetOneResult <RecordType extends Record = any> {
    data: RecordType;
}

export interface GetManyParams /*<RecordType extends Record = any>*/ {
    ids: Identifier[];
}
export interface GetManyResult <RecordType extends Record = any> {
    data: RecordType[];
}

export interface GetManyReferenceParams /*<RecordType extends Record = any>*/ {
    target: string;
    id: Identifier;
    pagination: Pagination;
    sort: Sort;
    filter: any;
 }

export interface GetManyReferenceResult<RecordType extends Record = any> {
    data: RecordType[];
    total: number;
    pageInfo?: {
        hasNextPage?: boolean;
        hasPreviousPage?: boolean;
    };
}

export interface UpdateParams<T = any> {
    id: Identifier;
    data: Partial<T>;
    previousData: T;
    //data: any;
    //previousData: Record;
}
export interface UpdateResult<RecordType extends RaRecord = any> {
    data: RecordType;
}

export interface UpdateManyParams<T = any> {
    ids: Identifier[];
    data: T;
    //data: any;
}
export interface UpdateManyResult <RecordType extends Record = any> {
    data?: RecordType['id'][];
    //data?: Identifier[];
}

export interface CreateParams<T = any> {
    data: T;
    //data: any;
}

export interface CreateResult <RecordType extends Record = any> {
    data: RecordType;
}

export interface DeleteParams<RecordType extends RaRecord = any> {
    id: RecordType['id'];
    previousData?: RecordType;
    //id: Identifier;
}
export interface DeleteResult<RecordType extends RaRecord = any> {
    data: RecordType;
    //data?: RecordType;
}

export interface DeleteManyParams<RecordType extends RaRecord = any> {
    ids: RecordType['id'][];
    //ids: Identifier[];
}

export interface DeleteManyResult<RecordType extends RaRecord = any> {
    data?: RecordType['id'][];
    //data?: Identifier[];
}

export type DataProviderProxy = {
    getList: (
        resource: string,
        params: GetListParams,
        options?: UseDataProviderOptions
    ) => Promise<GetListResult<RecordType>>;

    getOne: (
        resource: string,
        params: GetOneParams,
        options?: UseDataProviderOptions
    ) => Promise<GetOneResult<RecordType>>;

    getMany: (
        resource: string,
        params: GetManyParams,
        options?: UseDataProviderOptions
    ) => Promise<GetManyResult<RecordType>>;

    getManyReference: (
        resource: string,
        params: GetManyReferenceParams,
        options?: UseDataProviderOptions
    ) => Promise<GetManyReferenceResult<RecordType>>;

    update: (
        resource: string,
        params: UpdateParams,
        options?: UseDataProviderOptions
    ) => Promise<UpdateResult>;

    updateMany: (
        resource: string,
        params: UpdateManyParams,
        options?: UseDataProviderOptions
    ) => Promise<UpdateManyResult<RecordType>>;

    create: (
        resource: string,
        params: CreateParams,
        options?: UseDataProviderOptions
    ) => Promise<CreateResult<RecordType>>;

    delete: (
        resource: string,
        params: DeleteParams,
        options?: UseDataProviderOptions
    ) => Promise<DeleteResult<RecordType>>;

    deleteMany: (
        resource: string,
        params: DeleteManyParams,
        options?: UseDataProviderOptions
    ) => Promise<DeleteManyResult<RecordType>>;

    [key: string]: any;
};

export interface UseDataProviderOptions {
    action?: string;
    fetch?: string;
    meta?: object;
    undoable?: boolean;
    onSuccess?: any;
    onFailure?: any;
}
